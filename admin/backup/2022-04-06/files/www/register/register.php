<?php
require_once ("../connection/bddconnection.php");
session_start();
//// check to see if the user successfully created an account
//if (isset($_SESSION['username']) && isset($_SESSION['userid']))
//    header("Location: ./index.php");

?>

<script>
    function guardar() {
        let date = new Date();

        var name = jQuery("#username").val();
        var email = jQuery("#email").val();
        var password = jQuery("#password").val();
        var confirmerdPass = jQuery("#confirmPass").val()

        var params = {
            "name": name,
            "email": email,
            "password": password
        };
        console.log(params)
        validarEmail(email)

        if (comprobarContraseña(name, password, confirmerdPass) === true) {
            // alert("hola")
            $.ajax({
                data: params,
                type: 'POST',
                url: '/register/addNewUser.php',
                success: function (response) {

                    console.log(response)
                    setTimeout(3000);
                    window.location.href = "../login/login.php"
                }

            });
        }

    }

    function validarEmail(valor) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/.test(valor)){
            // alert("La dirección de email " + valor + " es correcta.");
        } else {
            // alert("La dirección de email es incorrecta.");
        }
    }


    function comprobarContraseña(name, password, confirmerdPass){

        if (name !== '' && password !== '' && confirmerdPass !== '' ){
            if (password === confirmerdPass){
                if (password.length >= 5 && strpbrk(password, "!#$.,:;()1234567890") !== false){
                    return true;
                }else{
                    alert("Tu contraseña no es lo suficientemente fuerte. Por favor utiliza otra")
                }
            }else{
                alert("Las contraseñas no coinciden.")
            }
        }else {
            alert("Por favor introduce los campos obligatorios.")
        }
    }

    function strpbrk(string, chars) {
        for(var i = 0, len = string.length; i < len; ++i) {
            if(chars.indexOf(string.charAt(i)) >= 0)
                return string.substring(i);
        }

        return false;
    }
</script>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Varela+Round&display=swap');

    /*html, body{*/
    /*    position: relative;*/
    /*    min-height: 100vh;*/
    /*    background: linear-gradient(10deg, #07cb8d, #ffffff);*/
    /*    display: block;*/
    /*    align-items: center;*/
    /*    justify-content: center;*/
    /*    font-family: "Fira Sans", Helvetica, Arial, sans-serif;*/
    /*    -webkit-font-smoothing: antialiased;*/
    /*    -moz-osx-font-smoothing: grayscale;*/
    /*}*/

    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Varela Round',sans-serif;
    }
    body{
        /*height: 100vh;*/
        /*display: block;*/
        /*justify-content: center;*/
        /*align-items: center;*/
        background : url("https://cdn5.f-cdn.com/contestentries/1578585/21468461/5d62b49ac544b_thumb900.jpg") no-repeat center center fixed;
        /*padding: 10px;*/
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        line-height: 1.5;
        margin: 0;
        min-block-size: 100vh;
        padding: 5vmin;
        /*background: linear-gradient(10deg, #07cb8d, #ffffff);*/
    }

    a{
        font-family: 'Varela Round',sans-serif;
    }

    .container{
        max-width: 700px;
        width: 100%;
        margin-left: 30%;
        margin-top: 10%;
        text-align: center;
        background: linear-gradient(10deg,#f98e02,#02c8a7);
        padding: 25px 30px;
        border-radius: 15px;
        box-shadow: 0 5px 10px rgba(0,0,0,0.15);
    }

    .container:hover{
        -webkit-animation: tiembla 0.35s;

    }
    @-webkit-keyframes tiembla{
        0%  { -webkit-transform:rotateZ(-2deg); }
        50% { -webkit-transform:rotateZ( 0deg) scale(.8); }
        100%{ -webkit-transform:rotateZ( 2deg); }
    }
    .container .title{
    /*    font-size: 25px;*/
    /*    font-weight: 500;*/
    /*    position: relative;*/

        z-index: -1;
        width: 400px;
        -webkit-box-shadow: 0 15px 25px #777;
        background: transparent !important;
        /*background-color: #F23545;*/
        border-top-left-radius: 1.25em;
        border-top-right-radius: 1.25em;
        color: black;
        padding: 1.25em 1.625em;
    }
    .content form .user-details{
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        margin: 20px 0 12px 0;
    }
    form .user-details .input-box{
        margin-bottom: 15px;
        border-radius: 10px;
        width: calc(100% / 2 - 30px);
        height: 30px;
    }

    form .button{
        height: 45px;
        margin: 35px 0
    }
    .button{
        height: 50%;
        width: 50%;
        border-radius: 5px;
        border: none;
        color: #fff;
        font-size: 18px;
        font-weight: 500;
        letter-spacing: 1px;
        cursor: pointer;
        transition: all 0.3s ease;
        background: linear-gradient(10deg, #07cb8d, #ffffff);
    }

    .button:hover{
        background: linear-gradient(10deg,#ffffff, #07cb8d)
    }


</style>


<!DOCTYPE html>
<html>
<head>
    <title>php member system | registration form</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrap.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!--        <link rel="stylesheet" href="https://static.alwaysdata.com/css/administration.css" type="text/css" media="all" />-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap">
</head>
<body>
<!--<h1>member system tutorial - register</h1>-->
<!--<a href="../index.php">Home</a> |-->
<!--<a href="./register.php">Register</a> |-->
<!--<a href="../login/login.php">Lo gin</a>-->
<!--<hr />-->

<!-- error message code here -->

<!-- registration form html code here -->

</body>


<form name="insert" action="" class="container" method="POST">

    <div class="title">
        <h2>Crear nueva cuenta</h2>
    </div>

    <div class="user-details">
        <input class="input-box" type="text" id="username" name="username" value="" placeholder="Usuario" autocomplete="off" required />
    </div>
    <div class="user-details">
        <input class="input-box" type="text" id="email" name="email" value="" placeholder="Correo" autocomplete="off" required />
    </div>
    <div class="user-details">
        <input class="input-box" type="password" id="password" name="passwd" value="" placeholder="Contraseña" autocomplete="off" required />
    </div>

    <div class="user-details">
        <input class="input-box" type="password" id="confirmPass" name="confirm_password" value="" placeholder="confirm your password" autocomplete="off" required />
    </div>

    <div class="user-details">
        <input  type="submit" class="button" id="crear" placeholder="crear" onclick="guardar()"/>
    </div>

    <p class="title"><br />
        Already have an account? <a href="../login/login.php">Login here</a>
    </p>
</form>

<?php
if (isset($success) && $success == true){
    echo '<font color="green">Yay!! Your account has been created. <a href="../login/login.php">Click here</a> to login!<font>';
}
// check to see if the error message is set, if so display it
else if (isset($error_msg))
    echo '<font color="red">'.$error_msg.'</font>';
else
    echo ''; // do nothing


?>
</html>
