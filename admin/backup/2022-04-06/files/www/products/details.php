<?php
require_once ("../connection/bddconnection.php");
session_start();
//// check to see if the user successfully created an account
//if (isset($_SESSION['username']) && isset($_SESSION['userid']))
//    header("Location: ./index.php");

?>

<script>
    function guardar() {
        let date = new Date();

        var name = jQuery("#username").val();
        var email = jQuery("#email").val();
        var password = jQuery("#password").val();
        var confirmerdPass = jQuery("#confirmPass").val()

        var params = {
            "name": name,
            "email": email,
            "password": password
        };
        console.log(params)
        validarEmail(email)

        if (comprobarContraseña(name, password, confirmerdPass) === true) {
            alert("hola")
            $.ajax({
                data: params,
                type: 'POST',
                url: '/register/addNewUser.php',
                success: function (response) {
                    console.log(response)
                    window.location.href = "../login/login.php"
                }

            });
        }

    }

    function validarEmail(valor) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/.test(valor)){
            alert("La dirección de email " + valor + " es correcta.");
        } else {
            alert("La dirección de email es incorrecta.");
        }
    }


    function comprobarContraseña(name, password, confirmerdPass){

        if (name !== '' && password !== '' && confirmerdPass !== '' ){
            if (password === confirmerdPass){
                if (password.length >= 5 && strpbrk(password, "!#$.,:;()1234567890") !== false){
                    return true;
                }else{
                    alert("Tu contraseña no es lo suficientemente fuerte. Por favor utiliza otra")
                }
            }else{
                alert("Las contraseñas no coinciden.")
            }
        }else {
            alert("Por favor introduce los campos obligatorios.")
        }
    }

    function strpbrk(string, chars) {
        for(var i = 0, len = string.length; i < len; ++i) {
            if(chars.indexOf(string.charAt(i)) >= 0)
                return string.substring(i);
        }

        return false;
    }
</script>

<!doctype html>
<html lang="en">
<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport"
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap">
</head>
<body>
<header>

</header>
<main>
<!--    <a href="http://toysandshare.alwaysdata.net/products/details.php">page</a>-->
    <div class="details_main">
        <div class="info_top">
            <?php
            $sql = 'SELECT * FROM products WHERE id = "'.$_GET['product'].'"';
            $result = pg_query($conn, $sql);
            $resultCheck = pg_num_rows($result);

            if ($result):
            if ($resultCheck>0):
            while ($product = pg_fetch_assoc($result)):
            ?>

            <img src="<?php echo $product['imageLink']?>" alt="" class="image-left">
            <div class="product_right">
                <div class="product_right__top">
                    <h1>Toy Car
                        “Hot Wheels”</h1>
                    <div class="product_price prod_info_line">
                        <p class="price_header info_line_header">Price:</p>
                        <p class="price_value info_line_value">
                            <?php if ($product['price']=0||$product['price']=null){
                                echo 0;
                            }else{
                                echo $product['price'];
                            }?>$
                        </p>
                    </div>
                    <div class="product_donator prod_info_line">
                        <p class="donator_header info_line_header">Donator:</p>
                        <p class="donator_value info_line_value">
                            <?php

                            $donatorSQL = "SELECT * FROM users WHERE id='$product[donatorID]'";
                            $donatorInfo = pg_query($conn, $donatorSQL);
                            $donatorResult = pg_fetch_assoc($donatorInfo);

                            echo $donatorResult['name'];
                            ?>
                        </p>
                    </div>
                    <div class="product_address prod_info_line">
                        <p class="address_header info_line_header">Address:</p>
                        <p class="address_value info_line_value"><?php echo $product['productLocation']?></p>
                    </div>
                    <div class="product_email prod_info_line">
                        <p class="email_header info_line_header">Email:</p>
                        <p class="email_value info_line_value"><a href="mailto:<?php echo $donatorResult['email']?>" class="mailto"><?php echo $donatorResult['email']?></a></p>
                    </div>
                </div>
                <a href="tel:+<?php echo $donatorResult['phoneNumber']?>" class="contact_donator"><button class="product_dm">Contact the donator</button></a>
            </div>
        </div>
        <p class="product_description">
            <?php echo $product['productDescription']?>
        </p>
    </div>
    <?php
    endwhile;
    endif;
    endif;
    ?>
</main>
</body>
</html>