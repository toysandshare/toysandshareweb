<?php
include_once ("../connection/functions.php");
require_once ("../connection/bddconnection.php");
session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register</title>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css"/>
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap">
</head>
<body class="register_body">
<main class="big_main">
    <div class="register_wrapper">
        <form name="insert" action="./addNewUser.php" class="register_form" method="POST">

            <h2 class="register_form__header">Register</h2>

            <input class="input-box" type="text" id="username" name="username" value="" placeholder="Username" autocomplete="off" required />
            <input class="input-box" type="text" id="email" name="email" value="" placeholder="Email" autocomplete="off" required />
            <input class="input-box" type="password" id="password" name="password" value="" placeholder="Password" autocomplete="off" required />

            <!--   TODO Repeat password -->
            <input class="input-box" type="password" id="confirmPass" name="confirm_password" value="" placeholder="Password confirmation" autocomplete="off" required />

            <button type="submit" class="button" id="submitRegisterButton">Submit</button>

            <div class="login_button">
                <p class="login_button__title">
                    Already have an account?
                </p>
                <a href="../login/login.php">Login here</a>
            </div>

        </form>
    </div>
</main>
</body>
<?php
if (isset($success) && $success == true){
    console_log("Your account has been created.");
}
// check to see if the error message is set, if so display it
else if (isset($error_msg))
    console_log($error_msg);
else
    echo '';
?>
</html>
