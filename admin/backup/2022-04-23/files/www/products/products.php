<?php
require_once ("../connection/bddconnection.php");
session_start();

if($_SESSION['auth']){
    $idUser = $_SESSION['id'];
    $query = "SELECT * FROM usuaris WHERE id = '$idUser'";
    $select = pg_query($conn, $query) or die(pg_result_error()());
    $data = pg_fetch_array($select);
}else{
    header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/login/login.php');
}
?>

<!--<style>
    main{
        background: linear-gradient(10deg, #02C8A7, white);
    }
    .products__content{
        width: 100%;
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        justify-content: center;
    }
    .product_link{
        width: 100%;
        height: 100%;
        font-size: 1.25vw;
    }
    .text_product__donator{
        padding-left: 2%;
    }
    .content__product_block{
        width: 10%;
        border-radius: 5%;
        text-align: center;
        padding: 0.3% 0.3% 0 0.3%;
        aspect-ratio: 0.825/1;
        margin: 1.5% 1.5% 1.5% 1.5%;
        font-family: 'Poppins',sans-serif;
        border: var(--golden-color) 3px solid;
        box-shadow: rgb(38, 57, 77) 0 10px 15px -10px;
        transition: width 0.6s, margin 0.6s, box-shadow 0.6s;
        background-color: #f53240;
    }
    .content__product_block:hover{
        width: 15%;
        margin: 2% .5% 2% .5%;
        transition: width 0.6s, margin 0.6s, box-shadow 0.6s;
    }
    .text_product__product_name{
        padding-left: 2%;
        font-weight: bold;
        padding-top: 10px;
        font-family: sans-serif;
    }
    .product_block__image{
        width: 100%;
        border-top-left-radius: 5%;
        border-top-right-radius: 5%;
    }
    .product_block__text{
        display: flex;
        margin-top: 2%;
        margin-bottom: 3%;
        aspect-ratio: 2/1;
        flex-direction: column;
        justify-content: center;
        /*align-items: flex-start;*/
        background-color: #f9be02;
        border-bottom-left-radius: 5%;
        border-bottom-right-radius: 5%;
    }
    .product_block__text{
        margin: 2.5% 0 2.5% 0;
    }
    .product_block__text p{
        font-size: 0.75vw;
        transition: font-size 0.6s;
    }
    .content__product_block:hover .product_block__text p{
        font-size: 1.25vw;
        transition: font-size 0.6s;
    }
</style>-->

<!doctype html>
<html lang="en">
<title>Products</title>
<meta charset="UTF-8">
<meta name="viewport"
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css" type="text/css" media="all" />
<link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap">
<body>
<?php include ("../menu/menu.php") ?>
<main class="products_main">
    <div class="products__content">
        <?php
        $sql = "SELECT * FROM products ORDER BY id;";
        $result = pg_query($conn, $sql);
        $resultCheck = pg_num_rows($result);

        if ($result):
            if ($resultCheck>0):
                while ($product = pg_fetch_assoc($result)):
                    ?>

                    <div class="content__product_block">
                        <a href="http://toysandshare.alwaysdata.net/products/details.php?product=<?php echo $product['id']?>" class="product_link">

                            <img src="<?php echo $product['image_link']?>" alt="horizontalCar" class="product_block__image">
                            <div class="product_block__text">
                                <p class="text_product__product_name"><?php echo $product['product_name']?></p>
                                <?php

                                $donatorSQL = "SELECT * FROM usuaris WHERE id=$product[donator_id]";
                                $donatorInfo = pg_query($conn, $donatorSQL);
                                $donatorResult = pg_fetch_assoc($donatorInfo);
                                ?>
                                <p class="text_product__donator"><?php echo $donatorResult['username']?></p>
                            </div>

                        </a>
                    </div>

                <?php
                endwhile;
            endif;
        endif;
        ?>
    </div>
</main>
</body>
</html>
