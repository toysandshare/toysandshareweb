<?php
require_once ("../connection/bddconnection.php");

session_start();
if($_SESSION['auth']){
    $idUser = $_SESSION['id'];
    $query = "SELECT * FROM usuaris WHERE id = '$idUser'";
    $select = pg_query($conn, $query) or die(pg_result_error()());
    $data = pg_fetch_array($select);
}else{
    header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/login/login.php');
}
?>


<!doctype html>
<html lang="en">
<head>
    <title>Details</title>
    <meta charset="UTF-8">
    <meta name="viewport"
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap">
</head>
<body class="details_body big_body">
<?php include ("../menu/menu.php") ?>
<main class="big_main">
    <div class="details_main">
        <div class="info_top">
            <?php
            $sql = 'SELECT * FROM products WHERE id = '.$_GET['product'].'';
            $result = pg_query($conn, $sql);
            $resultCheck = pg_num_rows($result);

            if ($result):
            if ($resultCheck>0):
            while ($product = pg_fetch_assoc($result)):
            ?>

            <img src="<?php echo $product['image_link']?>" alt="" class="image-left">
            <div class="product_right">
                <div class="product_right__top">
                    <h1><?php echo $product['product_name']?></h1>
                    <div class="product_price prod_info_line">
                        <p class="price_header info_line_header">Price:</p>
                        <p class="price_value info_line_value">
                            <?php if ($product['price']==0||$product['price']==null||$product['price']==""){
                                echo 0;
                            }else{
                                echo $product['price'];
                            }?>€
                        </p>
                    </div>
                    <div class="product_donator prod_info_line">
                        <p class="donator_header info_line_header">Donator:</p>
                        <p class="donator_value info_line_value">
                            <?php

                            $donID = $product['donator_id'];
                            $donatorSQL = "SELECT * FROM usuaris WHERE id = '$donID'";
                            $donatorInfo = pg_query($conn, $donatorSQL);
                            $donatorResult = pg_fetch_assoc($donatorInfo);

                            echo $donatorResult['username'];

                            $donNum = $donatorResult['phone_number']
                            ?>
                        </p>
                    </div>
                    <div class="product_address prod_info_line">
                        <p class="address_header info_line_header">Address:</p>
                        <p class="address_value info_line_value"><?php echo $product['product_location']?></p>
                    </div>
                    <div class="product_email prod_info_line">
                        <p class="email_header info_line_header">Email:</p>
                        <p class="email_value info_line_value"><a href="mailto:<?php echo $donatorResult['email']?>" class="mailto"><?php echo $donatorResult['email']?></a></p>
                    </div>
                </div>
                <a href="tel:+<?php echo $donNum?>" class="contact_donator"><button class="product_dm">Contact the donator</button></a>
            </div>
        </div>
        <p class="product_description">
            <?php echo $product['product_description']?>
        </p>
    </div>
    <?php
    endwhile;
    endif;
    endif;
    ?>
</main>
</body>
</html>