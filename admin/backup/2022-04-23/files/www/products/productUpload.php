<?php
require_once ("../connection/bddconnection.php");
include_once ("../connection/functions.php");

$lastProdID = pg_fetch_result(pg_query($conn, "SELECT id FROM products ORDER BY id desc limit 1"),0);
$prodID = $lastProdID+=1;
$title =  $_REQUEST['product_title'];
$desc = $_REQUEST['product_description'];
$location =  $_REQUEST['product_location'];
$price = $_REQUEST['price'];
$image = $_REQUEST['image_link'];
$donID = $_REQUEST['donator_id'];

echo $prodID;

$sql = 
    "INSERT INTO products (id, date_created, image_link, price, product_description, product_location, product_name, donator_id) VALUES
    ($prodID, current_date, '$image', $price, '$desc', '$location', '$title', $donID)";
// products (date_created, image_link, price, product_description, product_location, product_name, donator_id)


if(pg_query($conn, $sql)){
    $message = "Your product was successfully added!";
    echo "<script type='text/javascript'>alert('$message');</script>";

    header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/products/products.php');

} else{
    echo "ERROR: Hush! Sorry $sql. "
        . pg_last_error($conn);

    $idUser = $_SESSION['id'];

    $query = "SELECT * FROM users WHERE id = $idUser";

    $select = pg_query($conn, $query) or die(pg_last_error());

    $data = pg_fetch_array($select);


    header('Refresh: 2; URL=http://toysandshare.alwaysdata.net/products/newProduct.php?user=');
}

// Close connection
pg_close($conn);