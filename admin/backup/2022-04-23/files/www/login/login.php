<?php
include_once ("../connection/functions.php");
require_once ('../connection/bddconnection.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap">
</head>
<body class="login_body">
<main class="big_main">
    <div class="login_page_wrapper">
        <div class="login_block">
            <div class="login_block__header">
                <h2>Build a better World together</h2>
                <p>Sharing & Caring isn’t that difficult</p>
            </div>

            <div class="login_block_container">

                <div class="login__header">
                    <img src="http://toysandshare.alwaysdata.net/user/images/lockIcon.svg" alt="mySVG">
                    <h2>
                        Log in
                    </h2>
                </div>

                <form method="POST" id="acceso" action="./userlogin.php" class="login__form" name="signin-form">
                    <div class="form__element">
                        <label for="email">Email or Username</label>
                        <input id="email" type="email" name="email" placeholder="mail@address.com" required autocomplete="username"/>
                    </div>
                    <div class="form__element">
                        <label for="password">Password</label>
                        <input id="password" type="password" class="acceso" name="password" placeholder="Password" required autocomplete="current-password"/>
                    </div>
                    <button class="login_submit_button" type="submit" name="btn" value="login" >Log In</button>
                </form>

            </div>
        </div>
    </div>
</main>
</body>
</html>