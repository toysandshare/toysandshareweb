<?php
include_once ("../connection/functions.php");
require_once ("../connection/bddconnection.php");
session_start();

$email = htmlentities(trim($_POST['email']));
$password = htmlentities(trim($_POST['password'])); ## Es preferible que guarde tus password encriptados.

$select = "SELECT id,email,password FROM usuaris WHERE email = '$email'";

## Hacemos la consulta de verificacion.
$query = pg_query($conn,$select) or die(pg_result_error());

## Verificamos si regreso registro la consulta.
if(pg_num_rows($query)>0){
    $data = pg_fetch_array($query);
    if (password_verify($password, $data['password'])) {

        $_SESSION['auth'] = true;
        $_SESSION['id'] = $data['id'];
        header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/products/products.php');

    }else{
        echo "<script type='text/javascript'> alert('Password doesn\'t match') </script>";
        header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/login/login.php');
    }
} else {
    echo "<script type='text/javascript'> alert('Something went wrong. Try checking your email.') </script>";
    header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/login/login.php');
}