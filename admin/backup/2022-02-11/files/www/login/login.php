<?php
// login.php
require_once ('../connection/bddconnection.php');
session_start();

//// If user is logged in, redirect him to index.php
//if (isset($_SESSION['identity'])) {
//    header('Location: index.php');
//    exit;
//}
//
//// Check if form is submitted.
//$submitted = false;
//if ($_SERVER['REQUEST_METHOD']=='POST') {
//
//    $submitted = true;
//
//    // Extract form data.
//    $email = $_POST['email'];
//    $password = $_POST['password'];
//
//    // Authenticate user.
//    $authenticated = false;
//    if ($email=='admin@example.com' && $password=='Secur1ty') {
//        $authenticated = true;
//
//        // Save identity to session.
//        $_SESSION['identity'] = $email;
//
//        // Redirect the user to index.php.
//        header('Location: index.php');
//        exit;
//    }
//}
?>

<style>

    * {
        padding: 0;
        box-sizing: border-box;
    }

    body {
        margin: 50px auto;
        text-align: center;
        width: 800px;
    }

    h1 {
        font-family: 'Passion One', serif;
        font-size: 2rem;
        text-transform: uppercase;
    }

    label {
        width: 150px;
        font-family: 'Passion One ', serif;
        display: inline-flex;
        text-align: left;
        font-size: 1.5rem;
    }

    input {
        border-radius: 0.5em;
        border: 2px solid #000000;
        font-size: 1.5rem;
        font-weight: 100;
        font-family: 'Lato', serif;
        padding: 10px;
    }

    form {
        margin: 25px auto;
        padding: 20px;
        border: 5px solid #000000;
        width: 500px;
        background: #7ed9c8;
    }

    div.form-element {
        margin: 20px 0;
    }

    button {
        padding: 10px;
        font-size: 2vw;
        font-family: 'Passion One', serif;
        font-weight: 100;
        background: #f23545;
        color: #000000;
        border: 2px solid #000000;
    }

    p.success,
    p.error {
        color: white;
        font-family: lato, "Lato Black", serif;

        background: #055159;
        display: inline-block;
        padding: 2px 10px;
    }

    p.error {
        background: orangered;
    }

</style>
<!DOCTYPE html>
<html>
<head>
    <title>Login page</title>
</head>
<body>
<h1>Log in</h1>
<?php if ($submitted && !$authenticated): ?>
    <div class="alert">
        Invalid credentials.
    </div>
<?php endif; ?>
<?php if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
    //Valid email!

} ?>
<form method="post" action="/login.php" name="signin-form">
    <h2>Build a better World together</h2>
    <p>Sharing & Caring isn’t that difficult</p>
    <div class="form-element">
        <label>Email</label>
        <input type="text" name="email" required />
    </div>
    <div class="form-element">
        <label>Password</label>
        <input type="password" name="password" required />
    </div>
    <button type="submit" name="login-google" value="login-google">Log in with Google</button>
    <button type="submit" name="login" value="login" onclick="toPrincipalPage()">Log In</button>
</form>
</body>
</html>
