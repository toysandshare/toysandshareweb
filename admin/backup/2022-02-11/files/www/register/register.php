<?php
require_once ("../connection/bddconnection.php");
session_start();
// check to see if the user successfully created an account
//if (isset($_SESSION['username']) && isset($_SESSION['userid']))
//    header("Location: ./index.php");
//
//
//
//?>
<html>
<head>
    <title>php member system | registration form</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<script>
    function guardar() {
        let date = new Date();

        var name = jQuery("#username").val();
        var email = jQuery("#email").val();
        var password = jQuery("#password").val();
        var confirmerdPass = jQuery("#confirmPass").val()
        var fecha = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();

        var params = {
            "name": name,
            "email": email,
            "password": password,
            "fechaCreacion": fecha
        };
        console.log(params)

        if (comprobarContraseña(name, password, confirmerdPass) === true) {
            alert("hola")
            $.ajax({
                data: params,
                type: 'POST',
                url: './addNewUser.php',
                success: function (response) {
                    console.log(response)
                    alert("bdfb")
                }

            });
        }

    }

    function comprobarContraseña(name, password, confirmerdPass){

        if (name !== '' && password !== '' && confirmerdPass !== '' ){
            if (password === confirmerdPass){
                if (password.length >= 5 && strpbrk(password, "!#$.,:;()1234567890") !== false){
                    return true;
                }else{
                    alert("Tu contraseña no es lo suficientemente fuerte. Por favor utiliza otra")
                }
            }else{
                alert("Las contraseñas no coinciden.")
            }
        }else {
            alert("Por favor introduce los campos obligatorios.")
        }
    }

    function strpbrk(string, chars) {
        for(var i = 0, len = string.length; i < len; ++i) {
            if(chars.indexOf(string.charAt(i)) >= 0)
                return string.substring(i);
        }

        return false;
    }
</script>

<body>
<h1>member system tutorial - register</h1>
<a href="../index.php">Home</a> |
<a href="./register.php">Register</a> |
<a href="../login/login.php">Login</a>
<hr />

<!-- error message code here -->

<!-- registration form html code here -->

</body>
</html>


<form action="./register.php" class="form" >

    <h1>Crear nueva cuenta</h1>

    <div class="">
<!--        --><?php
//        // check to see if the user successfully created an account
//        if (isset($success) && $success == true){
//            echo '<p color="green">Yay!! Your account has been created. <a href="../login/login.php">Click here</a> to login!<p>';
//        }
//        // check to see if the error message is set, if so display it
//        else if (isset($error_msg))
//            echo '<p color="red">'.$error_msg.'</p>';
//        ?>
    </div>

    <div class="">
        <input type="text" id="username" name="username" value="" placeholder="Usuario" autocomplete="off" required />
    </div>
    <div class="">
        <input type="text" id="email" name="email" value="" placeholder="Correo" autocomplete="off" required />
    </div>
    <div class="">
        <input type="password" id="password" name="passwd" value="" placeholder="Contraseña" autocomplete="off" required />
    </div>
    <div class="">
        <p>password must be at least 5 characters and<br /> have a special character, e.g. !#$.,:;()</font></p>
    </div>
    <div class="">
        <input type="password" id="confirmPass" name="confirm_password" value="" placeholder="confirm your password" autocomplete="off" required />
    </div>

    <div class="">
        <button   onclick="guardar()">Crear Cuenta</button>
    </div>

    <p class="center"><br />
        Already have an account? <a href="../login/login.php">Login here</a>
    </p>
</form>

<?php
//if (isset($success) && $success == true){
//    echo '<font color="green">Yay!! Your account has been created. <a href="../login/login.php">Click here</a> to login!<font>';
//}
//// check to see if the error message is set, if so display it
//else if (isset($error_msg))
//    echo '<font color="red">'.$error_msg.'</font>';
//else
//    echo ''; // do nothing
//
//?>
