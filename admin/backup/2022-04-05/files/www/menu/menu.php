<?php
//session_start();
require_once ('../connection/bddconnection.php');

$query = 'SELECT * FROM usuaris WHERE id = "'.$_GET['user'].'"';
$select = pg_query($conn, $query) or die(pg_result_error()());
$data = pg_fetch_array($select);

?>
<script>
        function openNav() {
        document.getElementById("sideNavigation").style.width = "250px";
        document.getElementById("sideNavigation").style.borderTopRightRadius = "10px"
        document.getElementById("main").style.marginLeft = "250px";
    }

        function closeNav() {
        document.getElementById("sideNavigation").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
    }
</script>
<style>

    @import url('https://fonts.googleapis.com/css2?family=Varela+Round&display=swap');

    :root {
        --blue: #5e72e4;
        --indigo: #5603ad;
        --purple: #8965e0;
        --pink: #f3a4b5;
        --red: #f5365c;
        --orange: #fb6340;
        --yellow: #ffd600;
        --green: #2dce89;
        --teal: #11cdef;
        --cyan: #2bffc6;
        --white: #fff;
        --gray: #8898aa;
        --gray-dark: #32325d;
        --light: #ced4da;
        --lighter: #e9ecef;
        --primary: #5e72e4;
        --secondary: #f7fafc;
        --success: #2dce89;
        --info: #11cdef;
        --warning: #fb6340;
        --danger: #f5365c;
        --light: #adb5bd;
        --dark: #212529;
        --default: #055159;
        --white: #fff;
        --neutral: #fff;
        --darker: black;
        --breakpoint-xs: 0;
        --breakpoint-sm: 576px;
        --breakpoint-md: 768px;
        --breakpoint-lg: 992px;
        --breakpoint-xl: 1200px;
        --font-family-sans-serif: 'Varela Round', sans-serif;
        --font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, 'Liberation Mono', 'Courier New', monospace;
    }
    /* The side navigation menu */
    .sidenav {
        height: 100%; /* 100% Full-height */
        width: 0; /* 0 width - change this with JavaScript */
        position: fixed; /* Stay in place */
        z-index: 1; /* Stay on top */
        top: 0;
        left: 0;
        background:linear-gradient(50deg, #07cd8d, #f98e02 100%) !important;/* Black*/
        overflow-x: hidden; /* Disable horizontal scroll */
        padding-top: 60px; /* Place content 60px from the top */
        transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
    }


    /* The navigation menu links */
    .sidenav a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #342e2e;
        font-family: sans-serif;;
        display: block;
        transition: 0.3s
    }

    /* When you mouse over the navigation links, change their color */
    .sidenav a:hover, .offcanvas a:focus{
        color: #f1f1f1;
    }

    .navbar {

        position: relative;
        display: flex;
        padding: 1rem 1rem;
        flex-wrap: wrap;
        align-items: center;
        justify-content: space-between;
    }

    .navbar>.container,
    .navbar>.container-fluid {
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        justify-content: space-between;
    }

    .navbar-nav {
        display: flex;
        flex-direction: column;
        margin-bottom: 0;
        padding-left: 0;
        list-style: none;
    }

    .navbar-nav .nav-link {
        padding-right: 0;
        padding-left: 0;
    }

    .navbar-nav .dropdown-menu {
        position: static;
        float: none;
    }

    @media (max-width: 767.98px) {

        .navbar-expand-md>.container,
        .navbar-expand-md>.container-fluid {
            padding-right: 0;
            padding-left: 0;
        }
    }

    @media (min-width: 768px) {
        .navbar-expand-md {
            flex-flow: row nowrap;
            justify-content: flex-start;
        }

        .navbar-expand-md .navbar-nav {
            flex-direction: row;
        }

        .navbar-expand-md .navbar-nav .dropdown-menu {
            position: absolute;
        }

        .navbar-expand-md .navbar-nav .nav-link {
            padding-right: 1rem;
            padding-left: 1rem;
        }

        .navbar-expand-md>.container,
        .navbar-expand-md>.container-fluid {
            flex-wrap: nowrap;
        }
    }

    .navbar-dark .navbar-nav .nav-link {
        color: rgba(255, 255, 255, .95);
    }

    .navbar-dark .navbar-nav .nav-link:hover,
    .navbar-dark .navbar-nav .nav-link:focus {
        color: rgba(255, 255, 255, .65);
    }

    /* Position and style the close button (top right corner) */
    .sidenav .closebtn {
        position: absolute;
        top: 0;
        right: 25px;
        font-size: 36px;
        margin-left: 50px;
    }

    /* Style page content - use this if you want to push the page content to the right when you open the side navigation */
    #main {
        transition: margin-left .5s;
        padding: 20px;
        overflow:hidden;
        width:100%;
    }
    body {
        overflow-x: hidden;
    }

    /* Add a black background color to the top navigation */
    .topnav {
        /*background: linear-gradient(87deg, #F23545 0, #04BF9D 100%) !important;*/
        /*border-radius: 10px;*/
        /**/
        background: transparent;
        border-radius: 50px;
        overflow: hidden;
    }

    /* Style the links inside the navigation bar */
    .topnav a {
        float: left;
        display: block;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 17px;
    }

    /* Change the color of links on hover */
    .topnav a:hover {
        background-color: #ddd;
        color: black;
    }

    /* Add a color to the active/current link */
    .topnav a.active {
        background-color: #4CAF50;
        color: white;
    }

    /* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
    @media screen and (max-height: 450px) {
        .sidenav {padding-top: 15px;}
        .sidenav a {font-size: 18px;}
    }

    a svg{
        transition:all .5s ease;

    &:hover{
         #transform:rotate(180deg);
     }
    }

    #ico{
        display: none;
    }

    .menu{
        background: #000;
        display: none;
        padding: 5px;
        width: 320px;
        @include border-radius(5px);

        #transition: all 0.5s ease;

    a{
        display: block;
        color: #fff;
        text-align: center;
        padding: 10px 2px;
        margin: 3px 0;
        text-decoration: none;
        background: #0e0e0e;

    &:nth-child(1){
         margin-top: 0;
         @include border-radius(3px 3px 0 0 );
     }
    &:nth-child(5){
         margin-bottom: 0;
         @include border-radius(0 0 3px 3px);
     }

    &:hover{
         background: transparent;
     }
    }
    }


</style>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico"/>
    <link rel="stylesheet" href="css/estilos.css"/>
    <script src="scripts/require-jquery.js"></script>
    <link rel="stylesheet" href="css/estilo_menu.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

</head>
<body>
<header >
    <div id="sideNavigation" class="sidenav col-md-2">
        <header id="logo">
            <a href="../index.php">
                <img src=""/>
            </a>
        </header>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a class="bi bi-person" href="../user/user.php?user=<?= $data['id']?>">  Usuario</a>
        <a class="bi bi-list" href="../products/products.php?user=<?= $data['id']?>">  Productos</a>
        <a class="bi bi-info-circle-fill" href="#">  About us</a>
        <a class="bi bi-box-arrow-in-right" href="../login/login.php">  Log in</a>
        <a class="bi bi-box-arrow-in-right" href="../products/newProduct.php?user=<?= $data['id']?>">  Add product</a>
        <a class="bi bi-box-arrow-left" href="../login/login.php"> Exit</a>
    </div>
    <div>
        <div>
<!--            <nav class="topnav col-md-2">-->
<!--                <a href="#" onclick="openNav()">-->
<!--                    <svg width="30" height="30" id="icoOpen">-->
<!--                        <path d="M0,5 30,5" stroke="#000" stroke-width="5"/>-->
<!--                        <path d="M0,14 30,14" stroke="#000" stroke-width="5"/>-->
<!--                        <path d="M0,23 30,23" stroke="#000" stroke-width="5"/>-->
<!--                    </svg>-->
<!--                </a>-->
<!--            </nav>-->
            <nav class="navbar navbar-top navbar-expand-md navbar-dark topnav" id="navbar-main">
                <div class="container-fluid">
                    <a href="#" onclick="openNav()">
                        <svg width="30" height="30" id="icoOpen">
                            <path d="M0,5 30,5" stroke="#000" stroke-width="5"/>
                            <path d="M0,14 30,14" stroke="#000" stroke-width="5"/>
                            <path d="M0,23 30,23" stroke="#000" stroke-width="5"/>
                        </svg>
                    </a>
                    <form class="navbar-search navbar-search-dark form-inline mr-3 d-md-flex ml-lg-auto">
                        <div class="form-group mb-0">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input class="form-control" placeholder="Search" type="text">
                            </div>
                        </div>
                    </form>
                    <!-- User -->
                    <ul class="navbar-nav align-items-center d-none d-md-flex">
                        <?php
                        $select = 'SELECT * FROM usuaris WHERE id = "'.$_GET['user'].'"';
                        $query = pg_query($conn, $select) or die(pg_result_error()());
                        $resultCheck = pg_num_rows($query);
                        if ($query):
                            if (pg_num_rows($query)>0):
                                while ($user = pg_fetch_assoc($query)):
                                    ?>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="<?php echo $user['profile_image']?>">
                </span>
                                                <div class="media-body ml-2 d-none d-lg-block">
                                                    <span class="mb-0 text-sm  font-weight-bold"><?php echo $user['name']. " ". $user['lastname']?></span>
                                                </div>
                                            </div>
                                        </a>
                                        <!--                                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">-->
                                        <!--                                    <div class=" dropdown-header noti-title">-->
                                        <!--                                        <h6 class="text-overflow m-0">Welcome!</h6>-->
                                        <!--                                    </div>-->
                                        <!--                                    <a href="../examples/profile.html" class="dropdown-item">-->
                                        <!--                                        <i class="ni ni-single-02"></i>-->
                                        <!--                                        <span>My profile</span>-->
                                        <!--                                    </a>-->
                                        <!--                                    <a href="../examples/profile.html" class="dropdown-item">-->
                                        <!--                                        <i class="ni ni-settings-gear-65"></i>-->
                                        <!--                                        <span>Settings</span>-->
                                        <!--                                    </a>-->
                                        <!--                                    <a href="../examples/profile.html" class="dropdown-item">-->
                                        <!--                                        <i class="ni ni-calendar-grid-58"></i>-->
                                        <!--                                        <span>Activity</span>-->
                                        <!--                                    </a>-->
                                        <!--                                    <a href="../examples/profile.html" class="dropdown-item">-->
                                        <!--                                        <i class="ni ni-support-16"></i>-->
                                        <!--                                        <span>Support</span>-->
                                        <!--                                    </a>-->
                                        <!--                                    <div class="dropdown-divider"></div>-->
                                        <!--                                    <a href="#!" class="dropdown-item">-->
                                        <!--                                        <i class="ni ni-user-run"></i>-->
                                        <!--                                        <span>Logout</span>-->
                                        <!--                                    </a>-->
                                        <!--                                </div>-->
                                    </li>
                                <?php
                                endwhile;
                            endif;
                        endif;
                        ?>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

</header>
</body>
</html>