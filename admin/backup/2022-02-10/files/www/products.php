<?php require_once ("./connection/bddconnection.php") ?>

<!doctype html>
<html lang="en">
<head>
    <title>Products</title>
    <meta charset="UTF-8">
    <meta name="viewport"
    <link rel="stylesheet" href="/main.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
</head>
<style>
    .products__content{
        display: flex;
        flex-wrap: wrap;
    }
    .content__product_block{
        margin: 0 1.5% 0 1.5%;
        width: 30%;
    }
</style>
<body>
<header>
</header>
<main>
    <div class="products__content">
        <div class="content__product_block ">
            <img src="" alt="" class="product_block__image">
            <div class="product_block__text">
                <h1 class="text_product__product_name">Hot Wheels car</h1>
<!--                distance or address?-->
                <p class="text_product__distance">3km</p>

                <p class="text_product__donator">Some name like really</p>
                <p class="text_product__description">Some text description between 50 and 225 symbols</p>
            </div>
        </div>
        <div class="content__product_block ">
            <img src="" alt="" class="product_block__image">
            <div class="product_block__text">
                <h1 class="text_product__product_name">Hot Wheels car</h1>
                <!--                distance or address?-->
                <p class="text_product__distance">3km</p>

                <p class="text_product__donator">Some name like really</p>
                <p class="text_product__description">Some text description between 50 and 225 symbols</p>
            </div>
        </div>
        <div class="content__product_block ">
            <img src="" alt="" class="product_block__image">
            <div class="product_block__text">
                <h1 class="text_product__product_name">Hot Wheels car</h1>
                <!--                distance or address?-->
                <p class="text_product__distance">3km</p>

                <p class="text_product__donator">Some name like really</p>
                <p class="text_product__description">Some text description between 50 and 225 symbols</p>
            </div>
        </div>
        <div class="content__product_block ">
            <img src="" alt="" class="product_block__image">
            <div class="product_block__text">
                <h1 class="text_product__product_name">Hot Wheels car</h1>
                <!--                distance or address?-->
                <p class="text_product__distance">3km</p>

                <p class="text_product__donator">Some name like really</p>
                <p class="text_product__description">Some text description between 50 and 225 symbols</p>
            </div>
        </div>
        <div class="content__product_block ">
            <img src="" alt="" class="product_block__image">
            <div class="product_block__text">
                <h1 class="text_product__product_name">Hot Wheels car</h1>
                <!--                distance or address?-->
                <p class="text_product__distance">3km</p>

                <p class="text_product__donator">Some name like really</p>
                <p class="text_product__description">Some text description between 50 and 225 symbols</p>
            </div>
        </div>
    </div>
</main>
</body>
</html>
