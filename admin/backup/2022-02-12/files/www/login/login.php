<?php
// login.php
require_once ('../connection/bddconnection.php');
session_start();

?>

<style>

    * {
        padding: 0;
        box-sizing: border-box;
    }

    body {
        margin: 50px auto;
        text-align: center;
        width: 800px;
    }

    h1 {
        font-family: 'Passion One', serif;
        font-size: 2rem;
        text-transform: uppercase;
    }

    label {
        width: 150px;
        font-family: 'Passion One ', serif;
        display: inline-flex;
        text-align: left;
        font-size: 1.5rem;
    }

    input {
        border-radius: 0.5em;
        border: 2px solid #000000;
        font-size: 1.5rem;
        font-weight: 100;
        font-family: 'Lato', serif;
        padding: 10px;
    }

    form {
        margin: 25px auto;
        padding: 20px;
        border: 5px solid #000000;
        width: 500px;
        background: #7ed9c8;
    }

    div.form-element {
        margin: 20px 0;
    }

    button {
        padding: 10px;
        font-size: 2vw;
        font-family: 'Passion One', serif;
        font-weight: 100;
        background: #f23545;
        color: #000000;
        border: 2px solid #000000;
    }

    p.success,
    p.error {
        color: white;
        font-family: lato, "Lato Black", serif;

        background: #055159;
        display: inline-block;
        padding: 2px 10px;
    }

    p.error {
        background: orangered;
    }

</style>
<!DOCTYPE html>
<html>
<head>
    <title>Login page</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<h1>Log in</h1>
<?php //if ($submitted && !$authenticated): ?>
<!--    <div class="alert">-->
<!--        Invalid credentials.-->
<!--    </div>-->
<?php //endif; ?>
<?php //if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
//    //Valid email!
//
//} ?>
<form method="POST" id="acceso" action="/login.php" name="signin-form">
    <h2>Build a better World together</h2>
    <p>Sharing & Caring isn’t that difficult</p>
    <div class="form-element">
        <label>Email</label>
        <input id="emailUser" type="text" class="acceso" name="email" placeholder="Email" required />
    </div>
    <div class="form-element">
        <label>Password</label>
        <input id="passwoeUser" type="password" class="acceso" name="password" required />
    </div>
    <button type="submit" name="login-google" value="login-google">Log in with Google</button>
    <button type="submit" name="acceso" value="login">Log In</button>
</form>
<script>
    $("#acceso").on("submit", function(e){
        //Evitamos que se envíe por defecto
        e.preventDefault();
        //Creamos un FormData con los datos del mismo formulario
        var formData = new FormData(document.getElementById("acceso"));

        //Llamamos a la función AJAX de jQuery
        $.ajax({
            //Definimos la URL del archivo al cual vamos a enviar los datos
            url: '/login/userlogin.php',
            //Definimos el tipo de método de envío
            type: "POST",
            //Definimos el tipo de datos que vamos a enviar y recibir
            dataType: "HTML",
            //Definimos la información que vamos a enviar
            data: formData,
            //Deshabilitamos el caché
            cache: false,
            //No especificamos el contentType
            contentType: false,
            //No permitimos que los datos pasen como un objeto
            processData: false
        }).done(function(echo){
            //Una vez que recibimos respuesta
            //comprobamos si la respuesta no es vacía
            if (echo !== "") {
                //Si hay respuesta (error), mostramos el mensaje
                mensaje.html(echo);
                mensaje.slideDown(500);
            } else {
                //Si no hay respuesta, redirecionamos a donde sea necesario
                //Si está vacío, recarga la página
                window.location.replace("");
            }
        });
    });
</script>
</body>
</html>
