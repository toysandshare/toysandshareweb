<!doctype html>
<html lang="en">
<head>
    <title>Products</title>
    <meta charset="UTF-8">
    <meta name="viewport"
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <?php require_once ("../connection/bddconnection.php") ?>
</head>
<body>
<header>
</header>
<main>
    <a href="http://toysandshare.alwaysdata.net/products/products.php">page</a>
    <div class="products__content">

        <?php
        $sql = "SELECT * FROM products;";
        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);

        if ($result):
            if (mysqli_num_rows($result)>0):
                while ($product = mysqli_fetch_assoc($result)):
                    ?>

                    <a href="http://toysandshare.alwaysdata.net/products/details.php?product=<?php echo $product['id']?>" class="product_link"></a>
                    <div class="content__product_block">
                        <img src="https://rb.gy/nqjajv" alt="horizontalCar" class="product_block__image">
                        <div class="product_block__text">
                            <h1 class="text_product__product_name"><?php echo $product['productName']?></h1>
                            <!--                distance or address?-->
                            <p class="text_product__distance"><?php echo $product['productLocation']?></p>

<!--                            <?php
/*
                            $donatorSQL = "SELECT * FROM users WHERE id='3'";
                            $donatorInfo = mysqli_query($conn, $donatorSQL);
                            $donatorResult = mysqli_fetch_assoc($donatorInfo)

                            */?>
                            <p class="text_product__donator"><?php /*echo $donatorInfo['name']*/?></p>-->
                            <p class="text_product__description"><?php echo $product['productDescription']?>></p>
                        </div>
                    </div>

                <?php
                endwhile;
            endif;
        endif;
        ?>
        <div class="content__product_block">
            <img src="https://rb.gy/nqjajv" alt="horizontalCar" class="product_block__image">
            <div class="product_block__text">
                <h1 class="text_product__product_name">Hot Wheels car</h1>
                <!--                distance or address?-->
                <p class="text_product__distance">3km</p>

                <p class="text_product__donator">Some name like really</p>
                <p class="text_product__description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                    Aenean commodo ligula eget dolor. Aenean massa.
                    Cum sociis natoque penatibus et magnis dis parturient montes,
                    nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis.</p>
            </div>
        </div>
        <div class="content__product_block ">
            <img src="https://rb.gy/lg6eo2" alt="verticalCar" class="product_block__image">
            <div class="product_block__text">
                <h1 class="text_product__product_name">Hot Wheels car</h1>
                <!--                distance or address?-->
                <p class="text_product__distance">3km</p>

                <p class="text_product__donator">Some name like really</p>
                <p class="text_product__description">Some text description between 50 and 225 symbols</p>
            </div>
        </div>
        <div class="content__product_block ">
            <img src="https://rb.gy/jwunuq" alt="mountain" class="product_block__image">
            <div class="product_block__text">
                <h1 class="text_product__product_name">Hot Wheels car</h1>
                <!--                distance or address?-->
                <p class="text_product__distance">3km</p>

                <p class="text_product__donator">Some name like really</p>
                <p class="text_product__description">Some text description between 50 and 225 symbols</p>
            </div>
        </div>
        <div class="content__product_block ">
            <img src="" alt="" class="product_block__image">
            <div class="product_block__text">
                <h1 class="text_product__product_name">Hot Wheels car</h1>
                <!--                distance or address?-->
                <p class="text_product__distance">3km</p>

                <p class="text_product__donator">Some name like really</p>
                <p class="text_product__description">Some text description between 50 and 225 symbols</p>
            </div>
        </div>


        <!-- <div class="products__product">
            <div class="product__content_inside">
                <img src="img/item_165.jpg" alt="" class="content_inside__image">
                <a class="content_inside__button_link" href="common/product.php?product=<?php /*echo $product['density']*/?>">-->
    </div>
</main>
</body>
</html>
