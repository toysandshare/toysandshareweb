<?php
include_once ("../connection/functions.php");
require_once ("../connection/bddconnection.php");
session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register</title>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css"/>
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap">
</head>
<body class="register_body">
<main class="big_main">
    <div class="register_wrapper">
        <form name="insert" action="./addNewUser.php" class="register_form" method="POST">

            <h2 class="register_form__header">Register</h2>

            <input class="input-box" type="text" id="username" name="username" value="" placeholder="Username" autocomplete="off" required />
            <input class="input-box" type="text" id="email" name="email" value="" placeholder="Email" autocomplete="off" required />
            <input class="input-box" type="password" id="password" name="password" value="" placeholder="Password" autocomplete="off" required />

            <!--   TODO Repeat password -->
            <input class="input-box" type="password" id="confirmPass" name="confirm_password" value="" placeholder="Password confirmation" autocomplete="off" required />

            <button type="submit" class="button" id="submitRegisterButton">Submit</button>

            <div class="login_button">
                <p class="login_button__title">
                    Already have an account?
                </p>
                <a href="../login/login.php">Login here</a>
            </div>

        </form>
    </div>
</main>
</body>
<?php
if (isset($success) && $success == true){
    console_log("Your account has been created.");
}
// check to see if the error message is set, if so display it
else if (isset($error_msg))
    console_log($error_msg);
else
    echo '';
?>
</html>

<style>
    html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,font,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,audio,canvas,details,figcaption,figure,footer,header,hgroup,mark,menu,meter,nav,output,progress,section,summary,time,video
    {
        font-family: 'Varela Round', sans-serif;
        border:0;
        outline:0;
        font-size:100%;
        vertical-align:baseline;
        background:transparent;
        margin:0;
        padding:0
    }
    .register_body {
        background : url("https://cdn5.f-cdn.com/contestentries/1578585/21468461/5d62b49ac544b_thumb900.jpg") no-repeat center center fixed;
        margin: 0;
        min-block-size: 100vh;
        background-size: cover;
        -o-background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
    }
    /*Registration CSS*/
    .register_wrapper{
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        align-content: center;
        justify-content: center;
    }

    .register_form{
        width: 25%;
        display: flex;
        border-radius: 15px;
        align-items: center;
        flex-direction: column;
        padding: 1.25em 1.625em;
        background-color: transparent;
        transition: width 0.5s, height 0.5s;
        -webkit-box-shadow: 0 15px 25px #777;
        box-shadow: 0 5px 10px rgba(0,0,0,0.15);
    }

    .register_form:hover {
        width: 27%;
        transition: width 0.5s, height 0.5s;
    }

    .register_form__header{
        font-size: 1.75vw;
    }

    .register_form .input-box{
        width: 100%;
        margin-top: 2.5%;
        font-size: 0.85vw;
        border-radius: 10px;
        padding: 1.25% .5% 0 .5%;
    }

    .register_form .input-box:focus{
        outline: none;
    }

    #submitRegisterButton{
        width: 35%;
        padding: 1% 0;
        color: #fff;
        border: none;
        cursor: pointer;
        font-size: 1.2vw;
        margin-top: 2.5%;
        font-weight: bold;
        border-radius: 5px;
        letter-spacing: 1px;
        background-color: #07cb8d;
        transition: background-color 0.5s ease, color 0.5s, font-size 0.5s;
    }

    #submitRegisterButton:hover{
        color: #07cb8d;
        font-size: 1.5vw;
        background: #ffffff;
        transition: background-color 0.5s ease, color 0.5s, font-size 0.5s;
    }
    .login_button{
        display: flex;
        margin-top: 2.5%;
        font-weight: bold;
        align-items: center;
        flex-direction: column;
    }

    .login_button a{
        transition: font-size 0.5s;
    }

    .login_button a:hover{
        font-size: 1vw;
        transition: font-size 0.5s;
    }
</style>