<?php
include_once ("../connection/functions.php");
require_once ("../connection/bddconnection.php");

$lastLogin = 0;
$status = 1;

$username = $_POST['username'];

$email = $_POST['email'];

$password = $_POST['password'];

$encryptPass = password_hash($password, PASSWORD_BCRYPT);

$address =  null;
$city = null;
$country= null;
$date_created =  date('Y-m-d');
$description = null;
$lastname = null;
$postal_code =  null;
$profile_image =  null;

$sql = "SELECT * FROM usuaris WHERE email='$email'";
$result = pg_query($conn, $sql);
$resultCheck = pg_num_rows($result);

if ($result){
    if ($resultCheck>0){
        echo "<script type='text/javascript'> alert('User with this email already exists') </script>";
        header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/register/register.php');
    }else{
        if(!validateEmail($email)){
            echo "<script type='text/javascript'> alert('Please check if your email is written correctly') </script>";
            header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/register/register.php');
        }else{
            if (!preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $password)){
                echo "<script type='text/javascript'> alert('Your password must be at least 8 characters in length and must contain at least one number, one upper case letter, one lower case letter and one special character.') </script>";
                header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/register/register.php');
            }else{
                $insert= "INSERT INTO usuaris (id, address, city, country, date_created, description, email, last_login, lastname, name, password, postal_code, profile_image, status) VALUES
                (nextval('usuaris_sequence'), null, null, null, '$date_created', null, '$email', '$lastLogin', null, '$username', '$encryptPass', null, null, '$status')";
                pg_query($conn, $insert) or die(pg_result_error($conn));
                if ($insert){
                    echo "<script type='text/javascript'> alert('Your account has just been created') </script>";
                    header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/login/login.php');
                }else
                    echo "<script type='text/javascript'> alert('There was an error trying to create your account') </script>";
                    header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/register/register.php');
            }
        }
    }
}

