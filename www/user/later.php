<div class="row">
    <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
        <div class="card card-profile shadow">
            <div class="row justify-content-center">
                <div class="col-lg-3 order-lg-2">
                    <div class="card-profile-image">
                        <a href="#">
                            <?php
                            $select = "SELECT * FROM usuaris WHERE id = '$idUser'";
                            $query = pg_query($conn, $select) or die(pg_result_error());
                            $resultCheck = pg_num_rows($query);
                            if ($query):
                                if (pg_num_rows($query)>0):
                                    while ($user = pg_fetch_assoc($query)):
                                        ?>
                                        <img src="<?php echo $user['profile_image']?>" class="rounded-circle">
                                    <?php
                                    endwhile;
                                endif;
                            endif;
                            ?>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
            </div>
            <div class="card-body pt-0 pt-md-4">
                <div class="row">
                    <div class="col">
                        <?php
                        $select = "SELECT count(products_id) as total FROM usuaris_products WHERE usuaris_id = '$idUser'";
                        $query = pg_query($conn, $select) or die(pg_result_error());
                        $resultCheck = pg_num_rows($query);
                        if ($query):
                            if (pg_num_rows($query)>0):
                                while ($user = pg_fetch_assoc($query)):
                                    ?>
                                    <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                        <div>
                                            <span class="heading"><?php echo $user['total']?></span>
                                            <span class="description">Products</span>
                                        </div>
                                    </div>
                                <?php
                                endwhile;
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="text-center">
                    <?php
                    $select = "SELECT * FROM usuaris WHERE id = '$idUser'";
                    $query = pg_query($conn, $select) or die(pg_result_error());
                    $resultCheck = pg_num_rows($query);
                    if ($query):
                        if (pg_num_rows($query)>0):
                            while ($user = pg_fetch_assoc($query)):
                                ?>
                                <h3>
                                    <?php echo $user['username']." ". $user['lastname'] ?><span class="font-weight-light"></span>
                                </h3>

                                <div class="h5 font-weight-300">
                                    <i class="ni location_pin mr-2"></i><?php echo $user['country']?>
                                </div>
                                <div>
                                    <i class="ni education_hat mr-2"></i>Fecha de creación: <?php echo $user['date_created'] ?>
                                </div>
                                <hr class="my-4">
                                <p><?php echo $user['description'] ?></p>
                                <a href="#">Show more</a>
                            <?php
                            endwhile;
                        endif;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-8 order-xl-1">
        <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h3 class="mb-0">My account</h3>
                    </div>
                    <div class="col-4 text-right">
                        <form method="post" action="">
                            <input type="submit" class="btn btn-sm btn-primary" style="border-color: #F23545; background-color: #F23545;" name="submit" value="Eliminar Cuenta">
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?php
                $select = "SELECT * FROM usuaris WHERE id = '$idUser'";
                $query = pg_query($conn, $select) or die(pg_result_error());
                $resultCheck = pg_num_rows($query);
                if ($query):
                    if (pg_num_rows($query)>0):
                        while ($user = pg_fetch_assoc($query)):
                            ?>
                            <form>
                                <h6 class="heading-small text-muted mb-4">User information</h6>
                                <div class="pl-lg-4">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-email">Email address</label>
                                                <input type="email" id="input-email" class="form-control form-control-alternative" placeholder="Email" value="<?php echo $user['email']?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group focused">
                                                <label class="form-control-label" for="input-first-name">First name</label>
                                                <input type="text" id="input-first-name" class="form-control form-control-alternative" placeholder="First name" value="<?php echo $user['username']?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group focused">
                                                <label class="form-control-label" for="input-last-name">Last name</label>
                                                <input type="text" id="input-last-name" class="form-control form-control-alternative" placeholder="Last name" value="<?php echo $user['lastname']?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-4">
                                <!-- Address -->
                                <h6 class="heading-small text-muted mb-4">Contact information</h6>
                                <div class="pl-lg-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group focused">
                                                <label class="form-control-label" for="input-address">Address</label>
                                                <input id="input-address" class="form-control form-control-alternative" placeholder="Home Address" value="<?php echo $user['address']?>" type="text" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group focused">
                                                <label class="form-control-label" for="input-city">City</label>
                                                <input type="text" id="input-city" class="form-control form-control-alternative" placeholder="City" value="<?php echo $user['city']?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group focused">
                                                <label class="form-control-label" for="input-country">Country</label>
                                                <input type="text" id="input-country" class="form-control form-control-alternative" placeholder="Country" value="<?php echo $user['country']?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-country">Postal code</label>
                                                <input type="number" id="input-postal-code" class="form-control form-control-alternative" placeholder="Postal code" value="<?php echo $user['postal_code']?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-4">
                                <!-- Description -->
                                <h6 class="heading-small text-muted mb-4">About me</h6>
                                <div class="pl-lg-4">
                                    <div class="form-group focused">
                                        <label>About Me</label>
                                        <textarea rows="4" class="form-control form-control-alternative" placeholder="A few words about you ..."><?php echo $user['description']?></textarea>
                                    </div>
                                </div>
                            </form>
                        <?php
                        endwhile;
                    endif;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>