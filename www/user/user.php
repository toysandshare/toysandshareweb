<?php
require_once('../connection/bddconnection.php');
session_start();
if ($_SESSION['auth']) {
    $idUser = $_SESSION['id'];
    $query = "SELECT * FROM usuaris WHERE id = '$idUser'";
    $select = pg_query($conn, $query) or die(pg_result_error()());
    $data = pg_fetch_array($select);
} else {
    header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/login/login.php');
}
?>

<style>
    :root {
        --sky-color: #7CD8D5;
        --golden-color: #F9BE02;
        --brigth-coral-color: #F53240;
        --aquamarine-color: #02C8A7;
    }

    .user_body {
        display: flex;
        overflow-x: hidden;
        flex-direction: column;
        /*background: #e7e7e7;*/
        background: linear-gradient(90deg, var(--brigth-coral-color), var(--aquamarine-color));
        font-family: 'Varela Round', sans-serif;
    }

    .user_wrapper {
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: column;
    }

    .user_top_background {
        width: 100%;
        aspect-ratio: 2.25;
        background-size: cover;
        background-origin: content-box;
        background-position: center 25%;
        background-image: url(https://st3.depositphotos.com/20363444/33425/i/450/depositphotos_334250112-stock-photo-cropped-view-children-playing-wooden.jpg);
    }

    .user_wrapper__mask {
        /*top: 0;*/
        left: 0;
        width: 100%;
        opacity: 0.8;
        aspect-ratio: 2.25;
        position: absolute;
        background: linear-gradient(90deg, var(--brigth-coral-color), var(--aquamarine-color));
    }

    .user_content {
        width: 97%;
        display: flex;
        padding: 0 1.5%;
        position: absolute;
        flex-direction: column;
    }

    .user_content__header {
        width: 55%;
        color: #f2f2f2;
        margin-top: 5%;
        line-height: 1.75;
    }

    .user_content__header h1 {
        font-size: 2.5vw;
    }

    .user_content__header p {
        font-size: 1vw;
        margin-bottom: 5%;
    }

    .user_content__header a {
        color: #f2f2f2;
        cursor: pointer;
        font-size: 1.5vw;
        padding: 1.5% 2.5%;
        border-radius: 15px;
        text-decoration: none;
        font-family: 'Varela Round', sans-serif;
        background-color: var(--aquamarine-color);
        box-shadow: rgb(38, 57, 77) 0px 5px 7.5px -10px;
        transition: box-shadow 0.5s, font-size 0.5s;
    }

    .user_content__header a:hover {
        font-size: 1.65vw;
        box-shadow: rgb(38, 57, 77) 0px 10px 15px -10px;
        transition: box-shadow 0.5s, font-size 0.5s;
    }

    .user_information {
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: space-between;
        align-items: start;
    }

    .user_information div {
        padding: .5%;
        border-radius: 10px;
        background-color: #f2f2f2;
    }

    .user_main_block {
        width: 65%;
        display: flex;
        margin: 3% 0 2.5%;
        flex-direction: column;
        box-shadow: rgb(38, 57, 77) 0 10px 15px -10px;
    }

    .user_main_block div {
        display: flex;
        flex-direction: column;
    }

    .user_main_block .title {
        color: #919191;
        font-size: 0.7vw;
        margin-bottom: 1.5%;
        font-weight: bolder;
    }

    .user_main_block label {
        color: #333333;
        font-size: 0.85vw;
        margin-bottom: 1%;
        font-weight: bolder;
    }

    .user_main_block input, .user_main_block textarea {
        font-size: 0.9vw;
        padding: 3% .75%;
        border-radius: 5px;
        font-family: 'Varela Round', sans-serif;
    }

    .user_main_block textarea {
        padding: .5% .5%;
        resize: none;
    }

    .user_main_block .big_address {
        padding: 1% .75%;
    }

    .user_main_block__top_line {
        font-size: 1vw;
        font-weight: bold;
        align-items: center;
        justify-content: space-between;
        flex-direction: row !important;
    }

    .user_main_block__top_line button {
        padding: .5%;
        color: #f2f2f2;
        font-size: 0.9vw;
        border-radius: 5px;
        text-decoration: none;
        font-family: 'Varela Round', sans-serif;
        background-color: var(--brigth-coral-color);
    }

    .user_main_block__basic_info .rows {
        display: flex;
        flex-direction: row;
    }

    .user_main_block__basic_info .rows .basic_info__row {
        width: 100%;
        display: flex;
        flex-direction: column;
    }

    .user_main_block__address_info {
        width: 99.5%;
        display: flex;
        flex-direction: column;
    }

    .user_main_block__address_info .rows {
        display: flex;
        flex-direction: row;
    }

    .user_main_block__address_info .rows .address_info__row {
        width: 100%;
        display: flex;
        flex-direction: column;
    }

    .user_right_block {
        width: 30%;
        aspect-ratio: 1.45;
        display: flex;
        margin-top: 3%;
        text-align: center;
        align-items: center;
        flex-direction: column;
    }

    .profile_image {
        width: 100%;
        display: flex;
        justify-content: center;
    }

    .profile_image img {
        width: 12.5%;
        margin-top: -7%;
        position: absolute;
        border-radius: 50% !important;
        transition: width 0.5s, box-shadow 0.5s;
        box-shadow: rgb(38, 57, 77) 0 10px 15px -10px;
    }

    .profile_image img:hover {
        width: 13.5%;
        margin-top: -7%;
        position: absolute;
        border-radius: 50% !important;
        transition: width 0.5s, box-shadow 0.5s;
        box-shadow: rgb(38, 57, 77) 0 13px 18px -10px;
    }

    .profile_products_counter {
        font-size: 1vw;
        margin-top: 23%;
    }

    .profile_products_counter__header {
        font-weight: bolder;
    }

    .counter_text {
        color: #919191;
        margin-top: 10%;
        font-size: 0.8vw;
    }

    .profile_name {
        color: #333333;
        margin-top: 3%;
        font-size: 1.2vw;
        font-weight: bolder;
    }

    .country {
        color: #919191;
        margin-top: 3%;
        font-size: 0.9vw;
    }

    .creation_date {
        color: #333333;
        margin: 3% 0;
        font-size: 1vw;
    }

    .status {
        margin: 3% 0;
        color: #333333;
        font-size: 1.2vw;
        font-weight: bolder;
    }

    .show_more {
        margin-top: ;
        color: #ea2e3c;
        font-size: 1vw;
        cursor: pointer;
        font-weight: bolder;
        text-decoration: none;
        font-family: 'Varela Round', sans-serif;
    }

    .footer {
        display: flex;
        padding: 2.5rem 0;
        background: #f7fafc;
        justify-content: center;
        margin: 0 -1.5% !important;
    }

    .footer_link {
        color: #8898aa !important;
    }

    .footer_link:hover {
        color: #525f7f !important;
    }

    .separator {
        margin: 0 !important;
        padding: 0 !important;
        height: 1px;
        width: 100%;
        opacity: 0.5;
        background-color: #797979 !important;
    }

</style>

<!DOCTYPE html>
<html lang="en">
<!--<head>-->
<title>User page</title>
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet'
      type='text/css'>
<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css" type="text/css" media="all"/>
<link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png"/>
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap">
<!--</head>-->
<body class="user_body">
<?php include("../menu/menu.php") ?>
<main class="big_main">

    <div class="user_wrapper">
        <!--        --><?php //include ("../menu/menu.php") ?>
        <span class="user_wrapper__mask"></span>
        <div class="user_top_background"></div>
        <?php
        $select = "SELECT * FROM usuaris WHERE id = '$idUser'";
        $query = pg_query($conn, $select) or die(pg_last_error());
        $resultCheck = pg_num_rows($query);
        if ($query):
        if (pg_num_rows($query) > 0):
        while ($user = pg_fetch_assoc($query)):
        ?>

        <div class="user_content">
            <div class="user_content__header">
                <h1>Hello <?php echo $user['name'] ?>!</h1>
                <p>This is your profile page. Here you can see the information about your profile and products you have
                    uploaded</p>
                <a href="./editUser.php?user=<?php echo $_SESSION['id'] ?>">Edit Profile</a>
            </div>

            <div class="user_information">

                <div class="user_main_block">

                    <div class="user_main_block__top_line">

                        <p>My account</p>
                        <button>Delete account</button>

                    </div>

                    <div class="user_main_block__basic_info">

                        <p class="title">USER INFORMATION</p>

                        <div class="rows">
                            <div class="basic_info__row">
                                <label for="email">Email address</label>
                                <input name="email" id="email" type="text" value="<?php echo $user['email'] ?>"
                                       disabled>
                            </div>
                            <div class="basic_info__row">
                                <label for="username">First name</label>
                                <input name="username" id="username" type="text" value="<?php echo $user['name'] ?>"
                                       disabled>
                            </div>
                            <div class="basic_info__row">
                                <label for="lastname">Last name</label>
                                <input name="lastname" id="lastname" type="text" value="<?php echo $user['lastname'] ?>"
                                       placeholder="Your last name" disabled>
                            </div>
                        </div>

                    </div>

                    <div class="separator"></div>

                    <div class="user_main_block__address_info">

                        <p class="title">CONTACT INFORMATION</p>

                        <div class="address_info__row">
                            <label for="address">Address</label>
                            <input class="big_address" name="address" id="address" type="text" value="<?php
                            if ($user['address'] == null || $user['address'] == "" || $user['address'] == " ") {
                                echo "Your address";
                            } else {
                                echo $user['address'];
                            }
                            ?>" disabled>
                        </div>
                        <div class="rows">
                            <div class="address_info__row">
                                <label for="city">City</label>
                                <input name="city" id="city" type="text" value="<?php
                                if ($user['city'] == null || $user['city'] == "" || $user['city'] == " ") {
                                    echo "Your city";
                                } else {
                                    echo $user['city'];
                                }
                                ?>" disabled>
                            </div>
                            <div class="address_info__row">
                                <label for="country">Country</label>
                                <input name="country" id="country" type="text" value="<?php
                                if ($user['country'] == null || $user['country'] == "" || $user['country'] == " ") {
                                    echo "Your country";
                                } else {
                                    echo $user['country'];
                                }
                                ?>" disabled>
                            </div>
                            <div class="address_info__row">
                                <label for="postalCode">Postal Code</label>
                                <input name="postalCode" id="postalCode" type="text" value="<?php
                                if ($user['postal_code'] == null || $user['postal_code'] == "" || $user['postal_code'] == " ") {
                                    echo "Your postal_code";
                                } else {
                                    echo $user['postal_code'];
                                }
                                ?>" disabled>
                            </div>
                            <div class="address_info__row">
                                <label for="phone">Phone</label>
                                <input name="phone" id="phone" type="text" value="<?php
                                if ($user['phone'] == null || $user['phone'] == "" || $user['phone'] == " ") {
                                    echo "Your phone";
                                } else {
                                    echo $user['phone'];
                                }
                                ?>" disabled>
                            </div>
                        </div>

                    </div>

                    <div class="separator"></div>

                    <div class="user_main_block__user_description">

                        <p class="title">ABOUT ME</p>

                        <div class="user_description_row">

                            <label for="description">About Me</label>
                            <textarea name="description" id="description" cols="30" rows="10" placeholder="aboutMe"
                                      disabled><?php
                                if ($user['description'] == null || $user['description'] == "" || $user['description'] == " ") {
                                    echo "Your description";
                                } else {
                                    echo $user['description'];
                                }
                                ?></textarea>

                        </div>

                    </div>

                </div>

                <div class="user_right_block">

                    <a href="#" class="profile_image">
                        <img src="<?php echo $user['profile_image'] ?>" alt="profilePicture">
                    </a>

                    <div class="profile_products_counter">
                        <?php
                        $select = "SELECT count(id) as total FROM products WHERE usuari_id = '$idUser'";
                        $query = pg_query($conn, $select) or die(pg_result_error());
                        $resultCheck = pg_num_rows($query);
                        if ($query):
                            if (pg_num_rows($query) > 0):
                                while ($countingUser = pg_fetch_assoc($query)):
                                    ?>
                                    <p class="profile_products_counter__header"><?php echo $countingUser['total'] ?></p>
                                    <p class="counter_text">Products</p>
                                <?php
                                endwhile;
                            endif;
                        endif;
                        ?>
                    </div>

                    <p class="profile_name">
                        <?php
                        if (!$user['lastname'] == null || !$user['lastname'] == "" || !$user['lastname'] == " ") {
                            echo $user['name'], " ", $user['lastname'];
                        } else {
                            echo $user['name'];
                        }
                        ?>
                    </p>

                    <p class="country">
                        <?php echo $user['country'] ?>
                    </p>

                    <p class="creation_date">
                        Creation date: <?php echo $user['date_created'] ?>
                    </p>

                    <div class="separator"></div>

                    <p class="status">
                        <?php
                        if ($user['status'] == 1) {
                            echo "Active";
                        } else {
                            echo "Banned or abandoned";
                        }
                        ?>
                    </p>

                    <a class="show_more">Show more</a>

                </div>

            </div>

            <footer class="footer">
                <div class="copyright">
                    <p>Made by <a href="http://toysandshare.alwaysdata.net/" target="_blank" class="footer_link">Toys&Share</a>
                    </p>
                </div>
            </footer>
        </div>

    </div>
    <?php
    endwhile;
    endif;
    endif;
    ?>
</main>
</body>
</html>
