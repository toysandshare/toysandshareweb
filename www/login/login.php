<?php
include_once ("../connection/functions.php");
require_once ('../connection/bddconnection.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap">
</head>
<body class="login_body">
<main class="big_main">
    <div class="login_page_wrapper">
        <div class="login_block">
            <div class="login_block__header">
                <h2>Build a better World together</h2>
                <p>Sharing & Caring isn’t that difficult</p>
            </div>

            <div class="login_block_container">

                <div class="login__header">
                    <img src="http://toysandshare.alwaysdata.net/user/images/lockIcon.svg" alt="mySVG">
                    <h2>
                        Log in
                    </h2>
                </div>

                <form method="POST" id="acceso" action="./userlogin.php" class="login__form" name="signin-form">
                    <div class="form__element">
                        <label for="email">Email or Username</label>
                        <input id="email" type="email" name="email" placeholder="mail@address.com" required autocomplete="username"/>
                    </div>
                    <div class="form__element">
                        <label for="password">Password</label>
                        <input id="password" type="password" class="acceso" name="password" placeholder="Password" required autocomplete="current-password"/>
                    </div>
                    <button class="login_submit_button" type="submit" name="btn" value="login" >Log In</button>
                </form>

            </div>
        </div>
    </div>
</main>
</body>
</html>
<style>
    html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,font,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,audio,canvas,details,figcaption,figure,footer,header,hgroup,mark,menu,meter,nav,output,progress,section,summary,time,video
    {
        font-family: 'Varela Round', sans-serif;
        border:0;
        outline:0;
        font-size:100%;
        vertical-align:baseline;
        background:transparent;
        margin:0;
        padding:0
    }
    .login_body {
        background : url("https://cdn5.f-cdn.com/contestentries/1578585/21468461/5d62b49ac544b_thumb900.jpg") no-repeat center center fixed;
        margin: 0;
        min-block-size: 100vh;
        background-size: cover;
        -o-background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
    }
    .login_page_wrapper {
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        flex-direction: column;
        justify-content: center;
    }

    .login_block {
        width: 30%;
        display: flex;
        justify-items: center;
        flex-direction: column;
        justify-content: center;
    }

    .login_block__header{
        text-align: center;
    }

    .login_block__header h2{
        font-size: 1.75vw;
    }

    .login_block__header p{
        margin-top: 1.5%;
        font-size: 1.2vw;
    }

    .login__header {
        color: #000;
        display: flex;
        padding: 2.5%;
        font-size: 1vw;
        align-items: center;
        background: transparent;
        border-top-left-radius: 1.25em;
        border-top-right-radius: 1.25em;
        -webkit-box-shadow: 0 15px 25px #777;
    }

    .login__header img{
        width: 7.5%;
        margin-right: 1.75%;
    }

    .login__header h2{
        width: 100%;
        font-size: 1.75vw;
    }

    .login_block_container{
        margin-top: 2.5%;
    }

    .login__form {
        color: #777;
        display: flex;
        align-items: center;
        flex-direction: column;
        background: transparent;
        padding: 4% 1.625%;
        border-bottom-left-radius: 1.25%;
        border-bottom-right-radius: 1.25%;
        -webkit-box-shadow: 0 15px 25px #777;
    }

    .form__element{
        width: 80%;
        display: flex;
        margin-bottom: 2.5%;
        flex-direction: column;
    }

    .form__element label{
        font-size: 1.2vw;
        margin-bottom: .5%;
    }

    .login_block input {
        width: 100%;
        color: #055159;
        font-size: 1vw;
        margin-top: .75%;
        border-radius: 10px;
        padding: 1.25% 1.75%;
        background-color: #eee;
    }

    .login__form button{
        width: 30%;
        color: #fff;
        border: none;
        padding: 1% 0;
        cursor: pointer;
        margin-top: 1.5%;
        font-size: 1.25vw;
        font-weight: bold;
        border-radius: 5px;
        letter-spacing: 1px;
        background-color: #07cb8d;
        transition: background-color 0.5s ease, color 0.5s, font-size 0.5s;
    }

    .login__form button:hover{
        color: #07cb8d;
        font-size: 1.5vw;
        background: #ffffff;
        transition: background-color 0.5s ease, color 0.5s, font-size 0.5s;
    }

</style>