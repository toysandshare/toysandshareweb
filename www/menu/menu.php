<?php
require_once('../connection/bddconnection.php');

if ($_SESSION['auth']) {
    $idUser = $_SESSION['id'];
    $query = "SELECT * FROM usuaris WHERE id = '$idUser'";
    $select = pg_query($conn, $query) or die(pg_result_error()());
    $data = pg_fetch_array($select);
} else {
    echo "<script type='text/javascript'> alert('You should log in first!') </script>";
}
?>
<script>
    function openNav() {
        document.getElementById("sideNavigation").style.width = "250px";
        document.getElementById("sideNavigation").style.borderTopRightRadius = "10px"
        document.getElementById("main").style.marginLeft = "250px";
    }

    function closeNav() {
        document.getElementById("sideNavigation").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
    }
</script>

<style>
    html, body, ul, li, a, div, nav {
        margin: 0;
        padding: 0;
        color: #0a0a0a;
    }

    nav {
        width: 100%;
        height: 50px;
        background: transparent;
    }

    nav ul {
        list-style: none;
        display: flex;
        width: 100%;
    }

    nav ul li {
        position: relative;
        display: inline-block;
        width: 100%;
        height: 50px;
        float: left;
    }

    nav ul a {
        padding-left: 5px;
        padding-right: 5px;
        display: block;
        width: 100%;
        height: inherit;
        font-size: 1.2em;
        text-align: center;
        text-decoration: none;
        line-height: 50px;
        /*color: #FFF;*/
    }

    nav ul a:hover {
        background: white;
    }
    

    /*nav{*/
    /*    z-index: 1;*/
    /*    top: 5.5%;*/
    /*    right: 2.5%;*/
    /*    width: 100%;*/
    /*    display: flex;*/
    /*    font-size: 1.75vw;*/
    /*    position: absolute;*/
    /*    align-items: center;*/
    /*    font-weight: bolder;*/
    /*    justify-content: end;*/
    /*}*/
    /*nav img{*/
    /*    width: 10%;*/
    /*    border-radius: 50%;*/
    /*}*/
    /*.sidenav{*/
    /*    width: 100%;*/
    /*    display: flex;*/
    /*    flex-direction: row;*/
    /*    justify-content: end;*/
    /*}*/
    /*.sidenav p{*/
    /*    margin-left: 1.5%;*/
    /*    text-align: center;*/
    /*}*/
    /*.sidenav a{*/
    /*    text-align: center;*/
    /*    text-decoration: none;*/
    /*    font-family: 'Varela Round', sans-serif;*/
    /*}*/
    /*.somedipshit{*/
    /*    display: flex;*/
    /*    align-items: center;*/
    /*    background-color: darkgray;*/
    /*}*/
</style>

<header>
    <nav class="navbar navbar-top navbar-expand-md navbar-dark topnav" id="navbar-main">
        <!--        <div id="sideNavigation" class="sidenav">-->
        <!--            <div href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</div>-->
        <ul>
            <li><a class="bi bi-person" href="../user/user.php">User</a></li>
            <li><a class="bi bi-list" href="../products/products.php">Products</a></li>
            <li><a class="bi bi-info-circle-fill" href="#">About us</a></li>
            <li><a class="bi bi-box-arrow-in-right" href="../login/login.php">Log in</a></li>
            <li><a class="bi bi-box-arrow-in-right" href="../products/newProduct.php">Add product</a></li>
            <li><a class="bi bi-box-arrow-left" href="../connection/killSession.php">Exit</a></li>
        </ul>
        <!--        </div>-->
        <!--        <div>-->
        <!--            <div>-->
        <!--                <div class="container-fluid">-->
        <!--                                        <div class="openbtn" onclick="openNav()">-->
        <!--                                            <svg width="30" height="30" id="icoOpen">-->
        <!--                                                <path d="M0,5 30,5" stroke="#000" stroke-width="5"/>-->
        <!--                                                <path d="M0,14 30,14" stroke="#000" stroke-width="5"/>-->
        <!--                                                <path d="M0,23 30,23" stroke="#000" stroke-width="5"/>-->
        <!--                                            </svg>-->
        <!--                                        </div>-->
        <!--                    <form class="navbar-search navbar-search-dark form-inline mr-3 d-md-flex ml-lg-auto">-->
        <!--                        <div class="form-group mb-0">-->
        <!--                            <div class="input-group input-group-alternative">-->
        <!--                                <div class="input-group-prepend">-->
        <!--                                    <span class="input-group-text"><i class="fas fa-search"></i></span>-->
        <!--                                </div>-->
        <!--                                                                <input class="form-control" placeholder="Search" type="text">-->
        <!--                            </div>-->
        <!--                        </div>-->
        <!--                    </form>-->
        <!--                    User -->
        <!--                    <ul class="navbar-nav align-items-center d-none d-md-flex">-->
        <!--                        --><?php
        ///*                        $idUser = $_SESSION['id'];
        //                        $select = "SELECT * FROM usuaris WHERE id = '$idUser'";
        //                        $query = pg_query($conn, $select) or die(pg_result_error()());
        //                        $resultCheck = pg_num_rows($query);
        //                        if ($query):
        //                            if (pg_num_rows($query) > 0):
        //                                while ($user = pg_fetch_assoc($query)):
        //                                    */?>
        <!--                                    <li class="nav-item dropdown">-->
        <!--                                        <a class="nav-link pr-0" href="http://toysandshare.alwaysdata.net/user/user.php?user=-->
        <?php ///*echo $_SESSION['id']*/?><!--" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
        <!--                                            <div class="somedipshit">-->
        <!--                                                <img alt="Image placeholder" src="-->
        <?php ///*echo $user['profile_image'] */?><!--">-->
        <!--                                                <p class="mb-0 text-sm  font-weight-bold">-->
        <?php ///*echo $user['username'] . " " . $user['lastname'] */?><!--</p>-->
        <!--                                            </div>-->
        <!--                                        </a>-->
        <!--                                    </li>-->
        <!--                                --><?php
        ///*                                endwhile;
        //                            endif;
        //                        endif;
        //                        */?>
        <!--                    </ul>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
    </nav>
</header>