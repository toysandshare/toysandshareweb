<?php
require_once ("../connection/bddconnection.php");
session_start();

if($_SESSION['auth']){
    $idUser = $_SESSION['id'];
    $query = "SELECT * FROM usuaris WHERE id = '$idUser'";
    $select = pg_query($conn, $query) or die(pg_result_error()());
    $data = pg_fetch_array($select);
}else{
    header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/login/login.php');
}

?>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Varela+Round&display=swap');

    .new_product_header{
        font-family: 'Varela Round', sans-serif;
    }
    .new_product_form{
        display: flex;
        padding: 2.5%;
        border-radius: 30px;
        flex-direction: column;
        align-items: center;
        border: #fff 2px solid;
        background: transparent;
        box-shadow: rgb(38, 57, 77) 0 10px 15px -10px;
    }

    .new_product_form p input, .new_product_form p textarea{
        padding: 1%;
        margin-bottom: 1.5%;
        border-radius: 5px;
    }

    .button_to_upload{
        width: 30%;
        color: #000;
        cursor: pointer;
        font-size: 1.5vw;
        margin-top: 2.5%;
        font-weight: bold;
        border-radius: 5px;
        letter-spacing: 1px;
        border: 1px black solid;
        transition: color 0.5s, font-size 0.5s;
    }

    .button_to_upload:hover{
        color: #07cb8d;
        font-size: 1.75vw;
        transition: color 0.5s, font-size 0.5s;
    }

</style>

<!doctype html>
<html lang="en">
<head>
    <title>New product</title>
    <meta charset="UTF-8">
    <meta name="viewport"
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap">
</head>
<body>
<?php include ("../menu/menu.php") ?>
<main>
    <div class="new_product_wrapper">
        <h1 class="new_product_header">Here you can add your item to share!</h1>

        <form class="new_product_form" method="POST" action="http://toysandshare.alwaysdata.net/products/productUpload.php" enctype="multipart/form-data">
            <div class="main_form">
                <p class="title_input">
                    <label for="title">Product title:</label>
                    <input type="text" name="product_title" id="title">
                </p>

                <p class="description_input">
                    <label for="desc">Description:</label>
                    <textarea name="product_description" rows="4" cols="50" id="desc" wrap="soft"></textarea>
                </p>

                <p class="location_input">
                    <label for="location">Address:</label>
                    <input type="text" name="product_location" id="location">
                </p>

                <p class="price_input">
                    <label for="price">Price:</label>
                    <input type="text" name="price" id="price">
                </p>

                <p class="image_input">
                    <label for="image">Image link:</label>
                    <input type="text" name="image_link" id="image">
                </p>

                <p class="donID_input">
                    <label for="usuari_id">Your ID:</label>
                    <?php
                    $select = "SELECT * FROM usuaris WHERE id = $idUser";
                    $query = pg_query($conn, $select) or die(pg_last_error());
                    $resultCheck = pg_num_rows($query);
                    if ($query):
                        if ($resultCheck>0):
                            while ($user = pg_fetch_assoc($query)):
                                ?>
                                <input type="text" name="usuari_id" id="usuari_id" value="<?php echo $user['id']?>" readonly>
                            <?php
                            endwhile;
                        endif;
                    endif;
                    ?>
                </p>
            </div>
            <input type="submit" name="uploadBtn" value="Upload" class="button_to_upload" />
        </form>
    </div>
</main>
</body>
</html>
