<?php
require_once('../connection/bddconnection.php');
session_start();

$_SESSION['auth'] = null;
$_SESSION['id'] = null;

session_destroy();

header('Refresh: 0; URL=http://toysandshare.alwaysdata.net/login/login.php');